/**
 * Created on 6/9/18.
 */


app.controller('ProjectController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {
        
        $scope.projects = [];
        $scope.newproject = {};
        $scope.curproject = {};
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.uploaded = [];
        $scope.fileValidation = {};
        
        loadProject();
        loadCategory();

        $scope.uncheck = function(){
            $scope.newproject.featured = 0;
        };

        function loadProject() {
            $http.get($rootScope.base_url + 'dashboard/project/get').then(function (response) {
                if (response.data) {
                    $scope.projects = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        function loadCategory() {
            $http.get($rootScope.base_url + 'dashboard/category/get').then(function (response) {
                $scope.categories = response.data;
            });
        }

        $scope.newProject = function () {
            $scope.newproject = {};
            $scope.filespre = [];
            $scope.uploaded = [];
            $scope.files = [];
            $scope.errFiles = [];
            $scope.showform = true;
            $scope.item_files = false;
            $scope.curproject = false;
        };

        $scope.editProject = function (item) {
            $scope.files = [];
            $scope.uploaded = [];
            $scope.showform = true;
            $scope.curproject = item;
            $scope.newproject = angular.copy(item);
            $scope.item_files = item.files;
            // $scope.newproject.date = [];
            // if($scope.newproject.category == null) {
            //         $scope.newproject.category = [];
            //     }
        };

        $scope.hideForm = function () {
            $scope.errFiles = '';
            $scope.showform = false;
        };

        $scope.addProject = function () {
            // $scope.newsale.date = $filter('date')($scope.date, "yyyy-MM-dd");
            $scope.newproject.date = $filter('date')($scope.newproject.date, "yyyy-MM-dd");
            
            var fd = new FormData();
            angular.forEach($scope.newproject, function (item, key) {
                fd.append(key, item);
            });

            
           fd.append('uploaded', JSON.stringify($scope.uploaded));

            if ($scope.newproject['id']) {
                var url = $rootScope.base_url + 'dashboard/project/edit/' + $scope.newproject.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        $scope.projects.push(response.data);
                        loadProject();
                        $scope.newproject = {};
                        $scope.showform = false;
                        $scope.files = '';
                    },function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.files = '';
                    });
            } else {
                var url = $rootScope.base_url + 'dashboard/project/add';

                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        $scope.projects.push(response.data);
                        loadProject();
                        $scope.newproject = {};
                        $scope.showform = false;
                        $scope.files = [];

                    }, function onError(response) {
                        console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                        console.log(response.data);
                        $scope.validationError = response.data;
                    });
            }
        };

        $scope.deleteProject = function (item) {
              $ngConfirm({
                    title: 'Confirm!',
                    content: 'Would you like to delete this item ?',
                    type: 'red',
                    scope: $scope,
                    buttons: {
                        confirm: {
                            text: 'Yes',
                            btnClass: 'btn-danger',
                            action: function (scope, button) {
            var url = $rootScope.base_url + 'dashboard/project/delete/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    var index = $scope.projects.indexOf(item);
                    $scope.projects.splice(index, 1);
                    loadProject();
                },function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
                 }
                        },
                        close: function (scope, button) {
                            // closes the modal
                        }
                    }
                });
        };


        $scope.uploadFiles = function (files, errFiles) {
            angular.forEach(errFiles, function (errFile) {
                $scope.errFiles.push(errFile);
            });
            angular.forEach(files, function (file) {
                $scope.files.push(file);
                file.upload = Upload.upload({
                    url: $rootScope.base_url + 'dashboard/project/upload',
                    data: {file: file}
                });

                file.upload.then(function (response) {
                    $timeout(function () {
                        $scope.uploaded.push(response.data);
                        file.result = response.data;
                    });
                }, function (response) {
                    if (response.status > 0)
                        $scope.errorMsg = response.status + ': ' + response.data;
                }, function (evt) {
                    file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
                });
            });
        };

        $scope.deleteImage =function(item) {
            var url = $rootScope.base_url + 'dashboard/project/delete-image/' + item['id'];
            $http.delete(url)
                .then(function onSuccess(response) {
                    console.log('image deleted');
                    /*remove deleted file from scope variable*/
                    var index = $scope.item_files.indexOf(item);
                    $scope.item_files.splice(index, 1);
                },function onError(response) {
                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                    console.log(response.data);
                });
        };

        $scope.showProjectFiles = function (item) {
            console.log(item);
            $scope.projectfiles = item;
        };

          /****Modal***/

        $scope.animationsEnabled = true;

        $scope.showProject = function (project,size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'dashboard/viewProject',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return project;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        /****Modal end***/

        $scope.removeImage = function (image) {
            angular.forEach($scope.uploaded,function(item,key) {
                if (item.client_name == image.name) {
                    var index = $scope.files.indexOf(image);
                    $scope.files.splice(index, 1);
                    var index = $scope.uploaded.indexOf(item);
                    $scope.uploaded.splice(index, 1);
                    console.log($scope.uploaded);
                    console.log($scope.files);
                }
            })

        };


}]);





