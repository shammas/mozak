/**
 * Created by psybo-03 on 1/1/18.
 */


app.controller('CategoryController', [
    '$scope', '$http', '$rootScope', '$location', 'Upload', '$timeout', '$filter', '$uibModal', '$log', '$document', '$ngConfirm',
    function ($scope, $http, $rootScope, $location, Upload, $timeout, $filter, $uibModal, $log, $document, $ngConfirm) {

        $scope.categories = [];
        $scope.newcategory = {};
        $scope.curcategory = false;
        $scope.files = false;
        $scope.errFiles = [];
        $scope.showform = false;
        $scope.message = {};
        $rootScope.url = $location.path().replace('/', '');
        $scope.fileValidation = {};
        $scope.validationError = {};


        loadCategory();

        function loadCategory() {
            $http.get($rootScope.base_url + 'dashboard/category/get').then(function (response) {
                console.log(response.data);
                if (response.data) {
                    $scope.categories = response.data;
                    $scope.showtable = true;
                } else {
                    console.log('No data Found');
                    $scope.showtable = false;
                    $scope.message = 'No data found';
                }
            });
        }

        $scope.newCategory = function () {
            $scope.newcategory = {};
            $scope.filespre = [];
            $scope.files = false;
            $scope.errFiles = [];
            $scope.showform = true;
            
        };

        $scope.editCategory = function (item) {
            $scope.showform = true;
            $scope.curcategory = item;
            $scope.newcategory = angular.copy(item);
             };

        $scope.hideForm = function () {
            $scope.errFiles = [];
            $scope.showform = false;
        };

        $scope.addCategory = function () {

            var fd = new FormData();

            angular.forEach($scope.newcategory, function (item, key) {
                fd.append(key, item);
            });

            if ($scope.newcategory['id']) {
                var url = $rootScope.base_url + 'dashboard/category/edit/' + $scope.newcategory.id;
                $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined, 'Process-Data': false}
                })
                    .then(function onSuccess(response) {
                        loadCategory();
                        $scope.newcategory = {};
                        $scope.showform = false;
                        $scope.files = '';
                    }, function onError(response) {
                        console.log('edit Error :- Status :' + response.status + 'data : ' + response.data);
                        $scope.validationError = response.data;
                        $scope.files = '';
                    });
            } else {
               
                    var url = $rootScope.base_url + 'dashboard/category/add';
                    $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined, 'Process-Data': false}
                    })
                        .then(function onSuccess(response) {
                            loadCategory();
                            $scope.newcategory = {};
                            $scope.showform = false;
                            $scope.files = false;

                        }, function onError(response) {
                            console.log('addError :- Status :' + response.status + 'data : ' + response.data);
                            console.log(response.data);
                            $scope.validationError = response.data;
                            $scope.files = false;

                            if (response.status == 403) {
                                $scope.fileValidation.status = true;
                                $scope.fileValidation.msg = response.data.validation_error;
                            }
                        });
            }
        };

        $scope.deleteCategory = function (item) {
            $ngConfirm({
                title: 'Confirm!',
                content: 'Would you like to delete this item ?',
                type: 'red',
                scope: $scope,
                buttons: {
                    confirm: {
                        text: 'Yes',
                        btnClass: 'btn-danger',
                        action: function (scope, button) {
                            var url = $rootScope.base_url + 'dashboard/category/delete/' + item['id'];
                            $http.delete(url)
                                .then(function onSuccess(response) {
                                    var index = $scope.categories.indexOf(item);
                                    $scope.categories.splice(index, 1);
                                    loadCategory();
                                }, function onError(response) {
                                    console.log('Delete Error :- Status :' + response.status + 'data : ' + response.data);
                                    console.log(response.data);
                                });
                        }
                    },
                    close: function (scope, button) {
                        // closes the modal
                    }
                }
            });

        };

 /****Modal***/

        $scope.animationsEnabled = true;

        $scope.open = function (category, size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$scope',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return category;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);

