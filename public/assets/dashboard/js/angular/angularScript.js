/**
 * Created by psybo-03 on 13/07/18.
 */

var app = angular.module('mozak', ['ngRoute', 'ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngFileUpload', 'cp.ngConfirm', 'angular-loading-bar']);
app.config(['$routeProvider', '$locationProvider','cfpLoadingBarProvider', function ($routeProvider, $locationProvider,cfpLoadingBarProvider) {
    cfpLoadingBarProvider.spinnerTemplate = '<div id="loading"></div>';
    cfpLoadingBarProvider.latencyThreshold = 500;

    $locationProvider.hashPrefix('');
    $routeProvider
        .when('/', {
            templateUrl: 'dashboard/dashboard'
        })
        .when('/dashboard', {
            templateUrl: 'dashboard/dashboard',
            controller: 'DashboardController'
        })
        .when('/project', {
            templateUrl: 'dashboard/project',
            controller: 'ProjectController'
        })
        .when('/gallery', {
            templateUrl: 'dashboard/gallery',
            controller: 'GalleryController'
        })
        .when('/testimonial', {
            templateUrl: 'dashboard/testimonial',
            controller: 'TestimonialController'
        })
}]);

//Pagination filter
app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

