<?php
/**
 * Date: 06/09/18
 * Time: 2:14 PM
 */

defined('BASEPATH') or exit('No Direct Script Access Allowed');

class Project_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('File_model', 'file');
        // $this->load->model('Category_model', 'category');
        $this->load->model('Project_model', 'project');
        $this->load->model('Project_file_model', 'project_file');
        $this->load->library(['upload', 'image_lib','ion_auth']);

        $this->load->library('form_validation');
        $this->load->helper('url');

        if (!$this->ion_auth->logged_in()) {
            redirect(base_url('login'));
        }


    }
    function index()
    {
        $data = $this->project->with_files()->get_all();
        // $data = $this->project->with_files()->with_category()->get_all();
        if ($data != false) {
            foreach ($data as $value) {
                $value->files = array_values((array) $value->files);
            }
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function store()
    {
        $this->form_validation->set_rules('title', 'Title', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);

            unset($post_data['uploaded']);
            
            $project_id = $this->project->insert($post_data);

            if (!empty($uploaded) ) {
                /*INSERT FILE DATA TO DB*/
                foreach ($uploaded as $value) {
                    $file_data['file_name'] = $value->file_name;
                    $file_data['file_type'] = $value->file_type;
                    $file_data['size'] = $value->file_size;
                    $file_data['url'] = base_url() . 'uploads/';
                    $file_data['path'] = getcwd() . 'uploads/';

                    $file_id = $this->file->insert($file_data);

                    $project_file['project_id'] = $project_id;
                    $project_file['file_id'] = $file_id;

                    if ($this->project_file->insert($project_file)) {
                        /*****Create Thumb Image****/
                        $img_cfg['source_image'] = getcwd() . 'uploads/' . $value->file_name;
                        $img_cfg['maintain_ratio'] = TRUE;
                        $img_cfg['new_image'] = getcwd() . 'uploads/thumb/thumb_' . $value->file_name;
                        $img_cfg['quality'] = 99;
                        $img_cfg['master_dim'] = 'height';

                        $this->image_lib->initialize($img_cfg);
                        if (!$this->image_lib->resize()) {
                            $resize_error[] = $this->image_lib->display_errors();
                        }
                        $this->image_lib->clear();

                        /********End Thumb*********/

                        /*resize and create thumbnail image*/
                        if ($value->file_size > 1024) {
                            $img_cfg['image_library'] = 'gd2';
                            $img_cfg['source_image'] = getcwd() . 'uploads/' . $value->file_name;
                            $img_cfg['maintain_ratio'] = TRUE;
                            $img_cfg['new_image'] = getcwd() . 'uploads/' . $value->file_name;
                            $img_cfg['height'] = 500;
                            $img_cfg['quality'] = 100;
                            $img_cfg['master_dim'] = 'height';

                            $this->image_lib->initialize($img_cfg);
                            if (!$this->image_lib->resize()) {
                                $resize_error[] = $this->image_lib->display_errors();
                            }
                            $this->image_lib->clear();

                            /********End resize*********/
                        }
                    }
                    $resize_error = [];
                    if (empty($resize_error)) {
                        $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                    } else {
//                            $this->output->set_status_header(402, 'Server Down');
                        $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                    }
                }
            } else {
                $this->output->set_status_header(400, 'Validation Error');
                $this->output->set_content_type('application/json')->set_output(json_encode(['file' => 'Please select images.']));
            }
        }
    }

    function update($id){
        $this->form_validation->set_rules('title', 'Title', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);
            // $category = json_decode($post_data['category']);


            unset($post_data['uploaded']);
            // unset($post_data['category']);

            unset($post_data['files']);

            if (!empty($uploaded)) {
                /*INSERT FILE DATA TO DB*/
                foreach ($uploaded as $value) {
                    $file_data['file_name'] = $value->file_name;
                    $file_data['file_type'] = $value->file_type;
                    $file_data['size'] = $value->file_size;
                    $file_data['url'] = base_url() . 'uploads/';
                    $file_data['path'] = getcwd() . 'uploads/';

                    $file_id = $this->file->insert($file_data);

                    $project_file['file_id'] = $file_id;
                    $project_file['project_id'] = $id;

                    if ($this->project_file->insert($project_file)) {
                        /*****Create Thumb Image****/
                        $img_cfg['source_image'] = getcwd() . 'uploads/' . $value->file_name;
                        $img_cfg['maintain_ratio'] = TRUE;
                        $img_cfg['new_image'] = getcwd() . 'uploads/thumb/thumb_' . $value->file_name;
                        $img_cfg['quality'] = 99;
                        $img_cfg['master_dim'] = 'height';

                        $this->image_lib->initialize($img_cfg);
                        if (!$this->image_lib->resize()) {
                            $resize_error[] = $this->image_lib->display_errors();
                        }
                        $this->image_lib->clear();

                        /********End Thumb*********/

                        /*resize and create thumbnail image*/
                        if ($value->file_size > 1024) {
                            $img_cfg['image_library'] = 'gd2';
                            $img_cfg['source_image'] = getcwd() . 'uploads/' . $value->file_name;
                            $img_cfg['maintain_ratio'] = TRUE;
                            $img_cfg['new_image'] = getcwd() . 'uploads/' . $value->file_name;
                            $img_cfg['height'] = 500;
                            $img_cfg['quality'] = 100;
                            $img_cfg['master_dim'] = 'height';

                            $this->image_lib->initialize($img_cfg);
                            if (!$this->image_lib->resize()) {
                                $resize_error[] = $this->image_lib->display_errors();
                            }
                            $this->image_lib->clear();

                            /********End resize*********/
                        }
                    }
                }
                $this->project->update($post_data, $id);
                $resize_error = [];
                if (empty($resize_error)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                } else {
                    $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                }

                

                // if ($category != null) {
                //     foreach ($category as $val) {
                //         $cat_data['category'] = $value->category;
                //         $this->category->insert($cat_data);
                //     }
                // }
            } else
                if ($this->project->update($post_data, $id))
                    $this->output->set_content_type('application/json')->set_output(json_encode($post_data));

        }
    }

    function delete_image($id)
    {
        $project_file = $this->project_file->with_file()->where('file_id', $id)->get();

        if ($this->file->delete($project_file->file_id) and $this->project_file->delete($project_file->id)) {
            if (file_exists($project_file->file->path . $project_file->file->file_name)) {
                unlink($project_file->file->path . $project_file->file->file_name);
            }
            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Image Deleted']));
        }else{
            $this->output->set_status_header(400, 'Server Down');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later']));
        }
    }

    public function delete($id)
    {
        $project = $this->project->where('id',$id)->get();
        if ($project) {
            $project_files= $this->project_file->with_file()->where('project_id',$id)->get_all();
            if ($project_files) {
                foreach ($project_files as $file) {
                    if ($this->project_file->delete($file->id)) {
                        if ($this->file->delete($file->file_id)) {
                            if(file_exists(getcwd() . 'uploads/' . $file->file->file_name)){
                                unlink(getcwd() . 'uploads/' . $file->file->file_name);
                            }
                            $status = 1;
                        } else {
                            $status = 0;
                        }
                    }
                }
                if ($status == 1) {
                    if ($this->project->delete($id)) {
                            // $this->category->where('category_id', $id)->delete();

                        $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Project Deleted']));
                    }
                } elseif ($status == 0) {
                            // $this->project_categories->where('category_id', $id)->delete();
                        $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Project not deleted but some files are deleted']));
                }
            } else {
                if ($this->project->delete($id)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Project Deleted']));
                } else {
                    $this->output->set_status_header(500, 'Server Down');
                    $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Delete Error']));
                }
            }
        } else {
            $this->output->set_status_header(500, 'Server Down');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'The Record Not found']));
        }
    }


    function upload()
    {
        // $config['upload_path'] = getcwd() . 'uploads';
        $config['upload_path'] ='./uploads/';
        $config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
        $config['max_size'] = 4096;
        $config['file_name'] = 'PRJ_' . rand();
        $config['multi'] = 'ignore';
        $this->upload->initialize($config);
        if ($this->upload->do_upload('file')) {
            $this->output->set_content_type('application/json')->set_output(json_encode($this->upload->data()));
        }else{
            $this->output->set_status_header(401, 'File Upload Error');
            $this->output->set_content_type('application/json')->set_output($this->upload->display_errors('',''));
        }
    }
}