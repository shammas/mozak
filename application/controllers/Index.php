<?php
class Index extends CI_Controller {
    

        public function __construct()
        {
            parent::__construct();
            $this->load->helper('url_helper');
            $this->load->helper('url');
            $this->load->library('session');
            $this->load->helper('form');

            $this->load->model('Gallery_model', 'gallery');
            $this->load->model('Testimonial_model', 'testimonial');
            $this->load->model('Project_model', 'project');
            $this->load->model('Category_model', 'category');

        }

        protected $current = '';

        public function index()
        {
            $this->load->view('index');
        }

         public function about()
        {
            $data['testimonials'] = $this->testimonial->with_file()->get_all();
            $this->load->view('about',$data);
        }
        public function services()
        {
            $this->load->view('services');
        }
        public function projects()
        {
            $data['projects'] = $this->project->with_files()->with_category()->get_all();
            $data['categories'] = $this->category->get_all();

            $this->load->view('projects',$data);
        }
        public function gallery()
        {
            $data['galleries'] = $this->gallery->with_file()->get_all();
            $this->load->view('gallery',$data);
        }
        public function team()
        {
            $this->load->view('team');
        }
        public function contact()
        {
            $this->load->view('contact');
        }

        public function portfolio($id)
        {
            // $data['next'] = SELECT id FROM projects WHERE id = (select min(id) from projects where id > '5');
            // $data['previous'] =  SELECT id FROM projects WHERE id = (select max(id) from projects where id < '5');

            $data['next'] = $this->project->next($id);
            $data['previous'] = $this->project->previous($id);

            $data['project'] = $this->project->where('id',$id)->with_files()->get();
            if($data['project'] == false){
                show_404(); 
            }
            $this->load->view('portfolio-single',$data);
        }

}

