<?php
/**
 * 002_initial_schema.php
 * User: Noushid P
 * Date: 13/12/17
 * Time: 12:08 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Initial_schema extends CI_Migration {

    public function up()
    {
         /**
         * Table structure for table 'files'
         *
         */

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'file_type' => [
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => TRUE
            ],
            'size' => [
                'type' => 'INT',
                'constraint' => 10,
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'path' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('files');

         /**
         * Table structure for table 'projects'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'title' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            // 'category' => [
            //     'type' => 'LONGTEXT',
            //     'NULL'=>TRUE,
            // ],
            'category_id' => [
                'type' => 'INT',
                'constraint' => '8',
                'unsigned' => TRUE,
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'brief_description' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'date' => [
                'type'=>'DATE',
                'NULL'=>TRUE,
            ],
            'client' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'location' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
             'phone' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
                'NULL'=>TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('projects');

        /**
         * Table structure for table 'project_files'
         *
         */

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'project_id' => [
                'type' => 'INT',
                'constraint' => '8',
                'unsigned' => TRUE,
            ],
            'file_id' => [
                'type' => 'INT',
                'constraint' => '8',
                'unsigned' => TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('project_files');

        /**
         * Table structure for table 'categories'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'category' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('categories');


        /**
         * Table structure for table 'project_categories'
         *
         */

        // $this->dbforge->add_field([
        //     'id' => [
        //         'type' => 'INT',
        //         'constraint' => 5,
        //         'unsigned' => TRUE,
        //         'auto_increment' => TRUE
        //     ],
        //     'project_id' => [
        //         'type' => 'INT',
        //         'constraint' => '8',
        //         'unsigned' => TRUE,
        //     ],
        //     'category_id' => [
        //         'type' => 'INT',
        //         'constraint' => '8',
        //         'unsigned' => TRUE,
        //     ],
        //     'created_at' => [
        //         'type'=>'DATETIME',
        //         'NULL'=>TRUE,
        //     ],
        //     'updated_at' => [
        //         'type' => 'DATETIME',
        //         'NULL' => TRUE,
        //     ]
        // ]);
        // $this->dbforge->add_key('id', TRUE);
        // $this->dbforge->create_table('project_categories');


        /**
         * Table structure for table 'galleries'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_id' => [
                'type' => 'INT',
                'constraint' => '8',
                'unsigned' => TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('galleries');

        /**
         * Table structure for table 'testimonials'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'designation' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_id' => [
                'type' => 'INT',
                'constraint' => '8',
                'unsigned' => TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('testimonials');


        
        /**
         * Table structure for table 'ci_sessions'
         *
         **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'VARCHAR',
                'constraint' => 128,
            ],
            'ip_address' => [
                'type' => 'VARCHAR',
                'constraint' => 45,
            ],
            'timestamp' => [
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'default' => 0,
            ],
            'data' => [
                'type' => 'BLOB',
            ]
        ]);
        $this->dbforge->add_key(['id', 'ip_address'], TRUE);
        $this->dbforge->create_table('ci_sessions');
        $this->db->query("ALTER TABLE `ci_sessions` ADD KEY `ci_sessions_timestamp` (`timestamp`)");

    }

    public function down()
    {
        $this->dbforge->drop_table('files', TRUE);
        $this->dbforge->drop_table('galleries', TRUE);
        $this->dbforge->drop_table('testimonials', TRUE);
        $this->dbforge->drop_table('projects', TRUE);
        $this->dbforge->drop_table('categories', TRUE);
        // $this->dbforge->drop_table('project_categories', TRUE);
    }
}