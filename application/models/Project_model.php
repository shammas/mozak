<?php


defined('BASEPATH') or exit('No direct Script access allowed');
class Project_model extends MY_Model
{

    function __construct()
    {

        $this->has_many_pivot['files'] = array(
            'foreign_model'=>'file_model',
            'pivot_table'=>'project_files',
            'local_key'=>'id',
            'pivot_local_key'=>'project_id', /* this is the related key in the pivot table to the local key
                this is an optional key, but if your column name inside the pivot table
                doesn't respect the format of "singularlocaltable_primarykey", then you must set it. In the next title
                you will see how a pivot table should be set, if you want to  skip these keys */
            'pivot_foreign_key'=>'file_id', /* this is also optional, the same as above, but for foreign table's keys */
            'foreign_key'=>'id',
            'get_relate'=>TRUE /* another optional setting, which is explained below */
        );

        $this->has_one['category'] = array(
            'foreign_model' => 'Category_model',
             'foreign_table' => 'categories', 
             'foreign_key' => 'id', 
             'local_key' => 'category_id'
        );

        parent::__construct();
        $this->timestamps = TRUE;
    }
    public function next($id)
    {

        $this->db->select('*');
        $this->db->where("id = (select min(id) from projects where id > $id)");
        $query = $this->db->get('projects');
        if($query->num_rows() > 0 ){
            return $query->row();
        }
        else{
            return false;
        }
    }

     public function previous($id)
    {

        $this->db->select('*');
        $this->db->where("id = (select max(id) from projects where id < $id)");
        $query = $this->db->get('projects');
        if($query->num_rows() > 0 ){
            return $query->row();
        }
        else{
            return false;
        }
    }


}

