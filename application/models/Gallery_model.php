<?php
/**
 * Gallery_model.php
 * Date: 5/9/18
 * Time: 3:15 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Gallery_model extends MY_Model
{

    function __construct()
    {
        $this->has_one['file'] = array('foreign_model' => 'File_model', 'foreign_table' => 'files', 'foreign_key' => 'id', 'local_key' => 'file_id');
        parent::__construct();
        $this->timestamps = TRUE;
    }

}