<!DOCTYPE HTML>
<html lang="en">
<head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>About Firenze - Responsive  Architecture Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/yourstyle.css">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body>
        <!--================= main start ================-->
        <div id="main">
            <!--=============== header ===============-->
            <header class="main-header">
                <div class="scroll-holder">
                    <!-- logo-->
                    <div class="logo-holder">
                        <a href="index" class="ajax"><img src="images/logo.png" alt=""></a>
                        <p>Creative Theme for Architects</p>
                    </div>
                    <!-- logo end -->
                    <!-- navigation-->
                    <div class="nav-holder">
                        <nav>
                            <ul>
                                <li>
                                    <a href="index" class="ajax act-link">Home</a>
                                </li>
                                <li>
                                    <a href="about" class="ajax">About us </a>
                                </li>
                                <li>
                                    <a href="services" class="ajax">Services</a>
                                </li>
                                <li>
                                    <a href="projects" class="ajax">Projects</a>
                                </li>
                                <li>
                                    <a href="gallery" class="ajax">Gallery</a>
                                </li>
                                <li>
                                    <a href="team" class="ajax">Team</a>
                                </li>
                                <li>
                                    <a href="contact" class="ajax">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- navigation  end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Contact info</h3>
                        <ul class="header-contacts">
                            <li><span>Adress</span><a href="#">27th Brooklyn New York, NY 10065</a></li>
                            <li><span>Call</span><a href="#">+3 (123) 8976541</a></li>
                            <li><span>Write</span><a href="#">yourmail@domain.com</a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Find us</h3>
                        <ul class="header-social">
                            <li><a href="#" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i></a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <span class="close-menu"><i class="fa fa-times" aria-hidden="true"></i></span>
                </div>
                <!--footer-->
                <div class="header-footer">
                    &#169; 2018 / Designed by <a target="_blank" href="http://www.cloudbery.com/"><img src="images/cloudbery.png"></a><br>
                </div>
                <!-- footer end -->
            </header>
            <!-- header end -->
            <!-- nav-button-holder end -->
            <div class="nav-button-holder">
                <div class="nav-button">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!-- mobile logo-->
                <a class="mob-logo ajax" href="index"><img src="images/small-logo.png" alt=""></a>
            </div>
            <!-- nav-button-holder end -->
            <!--=============== wrapper ===============-->
            <div id="wrapper">
                <!-- content-holder  -->
                <div class="content-holder scale-bg2">
                    <!-- top-bar-holder  -->
                    <div class="top-bar-holder">
                        <div class="container">
                            <div class="top-bar">
                                <div class="top-bar-title">
                                    <h2><span>Page</span> : <a href="about" class="ajax">Our Team</a></h2>
                                </div>
                                <div class="show-share">
                                    <span>Share</span>
                                    <i class="fa fa-chain-broken"></i>
                                </div>
                                <div class="share-container"  data-share="['facebook','pinterest','googleplus','twitter','linkedin']"><a class="closeshare"><i class="fa fa-times"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <!-- top-bar-holder end  -->
                    <!-- wrapper-inner  -->
                    <div class="wrapper-inner">
                        <!-- content  -->
                        <div class="content">
                            <!-- page-title  -->
                            <div class="page-title">
                                <div class="overlay"></div>
                                <div class="slider-mask"></div>
                                <div class="bg"  style="background-image:url(images/bg/3.jpg)"></div>
                                <h2 data-top-bottom="transform: translateY(-150px);" data-bottom-top="transform: translateY(150px);">Our team</h2>
                                <div class="page-title-inner">
                                    <div class="container">
                                        <div class="page-title-decor">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- page-title end -->
                            <!-- container -->
                            <div class="container mr-bottom">
                                <!-- section  -->
                               <!-- section  team-->
                                <section class="md-padding" id="sec2">
                                    <div class="container small-container">
                                        <div class="inner-wrap no-margin ">
                                            <div class="section-title">
                                                <h3>Our team</h3>
                                            </div>
                                            <ul class="team-holder">
                                                <!-- 1 -->
                                                <li>
                                                    <div class="team-box">
                                                        <div class="team-photo">
                                                            <img src="images/team/small/1.jpg" alt="" class="respimg">
                                                        </div>
                                                        <div class="team-info">
                                                            <h4><a href="team-single" class="ajax">David Gray</a></h4>
                                                            <h5>CEO / Architect</h5>
                                                        </div>
                                                    </div>
                                                </li>
                                                <!-- 1 end -->
                                                <!-- 2 -->
                                                <li>
                                                    <div class="team-box">
                                                        <div class="team-photo">
                                                            <img src="images/team/small/2.jpg" alt="" class="respimg">
                                                        </div>
                                                        <div class="team-info">
                                                            <h4><a href="team-single" class="ajax">Andy Mansonn</a></h4>
                                                            <h5>Co-manager associated</h5>
                                                        </div>
                                                    </div>
                                                </li>
                                                <!-- 2 end -->
                                                <!-- 3 -->
                                                <li>
                                                    <div class="team-box">
                                                        <div class="team-photo">
                                                            <img src="images/team/small/4.jpg" alt="" class="respimg">
                                                        </div>
                                                        <div class="team-info">
                                                            <h4><a href="team-single" class="ajax">Jenny Smith</a></h4>
                                                            <h5>Architect / Designer</h5>
                                                        </div>
                                                    </div>
                                                </li>
                                                <!-- 3 end -->
                                                <!-- 4 -->
                                                <li>
                                                    <div class="team-box">
                                                        <div class="team-photo">
                                                            <img src="images/team/small/3.jpg" alt="" class="respimg">
                                                        </div>
                                                        <div class="team-info">
                                                            <h4><a href="team-single" class="ajax">Austin Evon</a></h4>
                                                            <h5>Architect</h5>
                                                        </div>
                                                    </div>
                                                </li>
                                                <!-- 4 end -->
                                                <!-- 5 -->
                                                <li>
                                                    <div class="team-box">
                                                        <div class="team-photo">
                                                            <img src="images/team/small/5.jpg" alt="" class="respimg">
                                                        </div>
                                                        <div class="team-info">
                                                            <h4><a href="team-single" class="ajax">Marta Roberts</a></h4>
                                                            <h5>Finance Manager </h5>
                                                        </div>
                                                    </div>
                                                </li>
                                                <!-- 5 end -->
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                                <!-- section  end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="to-top">
            <i class="fa fa-long-arrow-up"></i>
        </div>
            <!-- to top  end -->
    </div>
    <!-- Main end -->
    <!--  loader end -->
    <div class="loader">
        <div class="speeding-wheel"></div>
    </div>
    <!-- loader end -->
    <!--=============== google map ===============-->
    <script type="text/javascript"  src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <!--=============== scripts  ===============-->
    <script type="text/javascript"  src="js/jquery.min.js"></script>
    <script type="text/javascript"  src="js/plugins.js"></script>
    <script type="text/javascript"  src="js/core.js"></script>
    <script type="text/javascript"  src="js/scripts.js"></script>
    </body>
</html>