<!DOCTYPE HTML>
<html lang="en">
    
<head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Portfolio Firenze - Responsive  Architecture Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('css/reset.css');?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('css/plugins.css');?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('css/style.css');?>">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('css/yourstyle.css');?>">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="<?php echo base_url('images/favicon.ico');?>">
    </head>
    <body>
        <!--================= main start ================-->
        <div id="main">
            <!--=============== header ===============-->
            <header class="main-header">
                <div class="scroll-holder">
                    <!-- logo-->
                    <div class="logo-holder">
                        <a href="index" class="ajax"><img src="<?php echo base_url('images/logo.png');?>" alt=""></a>
                        <p>Creative Theme for Architects</p>
                    </div>
                    <!-- logo end -->
                    <!-- navigation-->
                    <div class="nav-holder">
                        <nav>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url('index');?>" class="ajax act-link">Home</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('about');?>" class="ajax">About us </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('services');?> " class="ajax">Services</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('projects');?> " class="ajax">Projects</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('gallery');?> " class="ajax">Gallery</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('team');?> " class="ajax">Team</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('contact');?> " class="ajax">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- navigation  end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Contact info</h3>
                        <ul class="header-contacts">
                            <li><span>Adress</span><a href="#">27th Brooklyn New York, NY 10065</a></li>
                            <li><span>Call</span><a href="#">+3 (123) 8976541</a></li>
                            <li><span>Write</span><a href="#">yourmail@domain.com</a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Find us</h3>
                        <ul class="header-social">
                            <li><a href="#" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i></a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <span class="close-menu"><i class="fa fa-times" aria-hidden="true"></i></span>
                </div>
                <!--footer-->
                <<div class="header-footer">
                    &#169; 2018 / Designed by <a target="_blank" href="http://www.cloudbery.com/"><img src="<?php echo base_url('images/cloudbery.png');?>"></a><br>
                </div>
                <!-- footer end -->
            </header>
            <!-- header end -->
            <!-- nav-button-holder end -->
            <div class="nav-button-holder">
                <div class="nav-button">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!-- mobile logo-->
                <a class="mob-logo ajax" href="<?php echo base_url('index');?>"><img src="<?php echo base_url('images/small-logo.png');?>" alt=""></a>
            </div>
            <!-- nav-button-holder end -->
            <!--=============== wrapper ===============-->
            <div id="wrapper">
                <!-- content-holder  -->
                <div class="content-holder scale-bg2">
                    <!-- top-bar-holder  -->
                    <div class="top-bar-holder">
                        <div class="container">
                            <div class="top-bar">
                                <div class="top-bar-title">
                                <?php
                            if (isset($project) and $project != false) {
                                    ?>
                                    <h2><span>Page</span> : <a href="" class="ajax"><?php echo $project->title;?></a></h2>
                                    <?php }  ?>
                                </div>
                                <div class="show-share">
                                    <span>Share</span>
                                    <i class="fa fa-chain-broken"></i>
                                </div>
                                <div class="share-container"  data-share="['facebook','pinterest','googleplus','twitter','linkedin']"><a class="closeshare"><i class="fa fa-times"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <!-- top-bar-holder  end-->
                    <!-- content-->
                    <div class="content hor-pad-con ">
                        <!-- resize-carousel-holder-->
                        <div class="resize-carousel-holder">
                            <div id="gallery_horizontal" class="gallery_horizontal" data-cen="0">
                            <?php
                            if (isset($project) and $project != false) {
                                    if (isset($project->files) and $project->files != false) {
                                        foreach ($project->files as $key => $file) {
                                          
                        ?>
                                <!-- gallery Item-->
                                <div class="horizontal_item" data-phname="Pellentesque fac" data-phdesc="A gray cat slinks past a wooden house.">
                                    <div class="box-item">
                                        <img src="<?php echo $file->url . $file->file_name;?>" alt="">
                                        <a class="slider-zoom image-popup" href="<?php echo $file->url . $file->file_name;?>"><i class="fa fa-expand"></i></a>
                                    </div>
                                </div>
                                <?php 
                            }   }   }  
                                ?>
                                
                            </div>
                            <div class="customNavigation">
                                <a class="next-slide transition"><i class="fa fa-long-arrow-right"></i></a>
                                <a class="prev-slide transition"><i class="fa fa-long-arrow-left"></i></a>
                            </div>
                        </div>
                        <!-- portfolio  Images  end-->
                        <div class="clearfix"></div>
                        <!-- container-->
                        <div class="container">
                            <section class="no-padding mr-bottom">
                                <div class="caption"></div>
                                <div class="num-holder2"></div>
                                <div class="details-box">
                                <?php
                            if (isset($project) and $project != false) {
                                    ?>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3><?php echo $project->title; ?></h3>
                                            <p class="big-parag"><?php echo $project->brief_description;?> </p>
                                            <p><?php echo $project->description;?></p>
                                            <a href="#" class="btn flat-btn float-btn ">launch project</a>
                                        </div>
                                        <div class="col-md-4">
                                            <ul class="det-list">
                                                <li><span>Date :</span> <?php echo date('d.m.Y', strtotime($project->date));?></li>
                                                <li><span>Client :</span>  <?php echo $project->client;?> </li>
                                                <li><span>Contact No. :</span> <?php echo $project->phone; ?> </li>
                                                <li><span>Location : </span>  <a href="https://www.google.com/maps/place/<?php echo $project->location;?>" target="_blank"> <?php echo $project->location;?>  </a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <?php 
                                }   
                                ?>
                                </div>
                                <div class="content-nav">
                                    <ul>
                                        <li>
                                            <?php 
                                                if (isset($previous) and $previous != false) {
                                            ?>
                                                <a href="<?php echo base_url('portfolio-single/' . $previous->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', $previous->title), '-');?>" class="ajax ln"><i class="fa fa fa-angle-left"></i></a> 

                                            <?php } ?>
                                        </li>
                                        <li>
                                            <div class="list">
                                                <a href="<?php echo base_url('projects');?>" class="ajax">
                                                <span>
                                                <i class="b1 c1"></i><i class="b1 c2"></i><i class="b1 c3"></i>
                                                <i class="b2 c1"></i><i class="b2 c2"></i><i class="b2 c3"></i>
                                                <i class="b3 c1"></i><i class="b3 c2"></i><i class="b3 c3"></i>
                                                </span></a>
                                            </div>
                                        </li>
                                        <li>
                                        <?php
                                            if (isset($next) and $next != false) {
                                        ?>
                                            <a href="<?php echo base_url('portfolio-single/' . $next->id . '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', $next->title), '-');?>" class="ajax rn"><i class="fa fa fa-angle-right"></i></a> 
                                        <?php } ?>
                                        </li>
                                    </ul>
                                </div>
                            </section>
                        </div>
                    </div>
                    <!-- container end-->
                </div>
                <!-- content-holder end -->
            </div>
            <!-- wrapper end -->
            <!--to top    -->
            <div class="to-top">
                <i class="fa fa-long-arrow-up"></i>
            </div>
            <!-- to top  end -->
        </div>
        <!-- Main end -->
        <!--  loader end -->
        <div class="loader">
            <div class="speeding-wheel"></div>
        </div>
        <!-- loader end -->
        <!--=============== google map ===============-->
        <script type="text/javascript"  src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <!--=============== scripts  ===============-->
        <script type="text/javascript"  src="<?php echo base_url('js/jquery.min.js');?>"></script>
        <script type="text/javascript"  src="<?php echo base_url('js/plugins.js');?>"></script>
        <script type="text/javascript"  src="<?php echo base_url('js/core.js');?>"></script>
        <script type="text/javascript"  src="<?php echo base_url('js/scripts.js');?>"></script>
    </body>

</html>