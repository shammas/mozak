<!DOCTYPE HTML>
<html lang="en">
    
<head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Portfolio Firenze - Responsive  Architecture Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/yourstyle.css">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body>
        <!--================= main start ================-->
        <div id="main">
            <!--=============== header ===============-->
            <header class="main-header">
                <div class="scroll-holder">
                    <!-- logo-->
                    <div class="logo-holder">
                        <a href="index" class="ajax"><img src="images/logo.png" alt=""></a>
                        <p>Creative Theme for Architects</p>
                    </div>
                    <!-- logo end -->
                    <!-- navigation-->
                    <div class="nav-holder">
                        <nav>
                            <ul>
                                <li>
                                    <a href="index" class="ajax act-link">Home</a>
                                </li>
                                <li>
                                    <a href="about" class="ajax">About us </a>
                                </li>
                                <li>
                                    <a href="services" class="ajax">Services</a>
                                </li>
                                <li>
                                    <a href="projects" class="ajax">Projects</a>
                                </li>
                                <li>
                                    <a href="gallery" class="ajax">Gallery</a>
                                </li>
                                <li>
                                    <a href="team" class="ajax">Team</a>
                                </li>
                                <li>
                                    <a href="contact" class="ajax">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- navigation  end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Contact info</h3>
                        <ul class="header-contacts">
                            <li><span>Adress</span><a href="#">27th Brooklyn New York, NY 10065</a></li>
                            <li><span>Call</span><a href="#">+3 (123) 8976541</a></li>
                            <li><span>Write</span><a href="#">yourmail@domain.com</a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Find us</h3>
                        <ul class="header-social">
                            <li><a href="#" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i></a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <span class="close-menu"><i class="fa fa-times" aria-hidden="true"></i></span>
                </div>
                <!--footer-->
                <div class="header-footer">
                    &#169; 2018 / Designed by <a target="_blank" href="http://www.cloudbery.com/"><img src="images/cloudbery.png"></a><br>
                </div>
                <!-- footer end -->
            </header>
            <!-- header end -->
            <!-- nav-button-holder end -->
            <div class="nav-button-holder">
                <div class="nav-button">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!-- mobile logo-->
                <a class="mob-logo ajax" href="index"><img src="images/small-logo.png" alt=""></a>
            </div>
            <!-- nav-button-holder end -->
            <!--=============== wrapper ===============-->
            <div id="wrapper">
                <!-- content-holder  -->
                <div class="content-holder scale-bg2">
                    <!-- top-bar-holder  -->
                    <div class="top-bar-holder fw-topbar">
                        <div class="top-bar">
                            <div class="top-bar-title">
                                <h2><span>Page</span> : <a href="portfolio2" class="ajax">Gallery</a></h2>
                            </div>
                            <div class="show-share">
                                <span>Share</span>
                                <i class="fa fa-chain-broken"></i>
                            </div>
                            <div class="share-container"  data-share="['facebook','pinterest','googleplus','twitter','linkedin']"><a class="closeshare"><i class="fa fa-times"></i></a></div>
                        </div>
                    </div>
                    <!-- top-bar-holder  end-->
                    <!-- content  -->
                    <div class="content pad-con2">
                        <!-- filter -->
                        <!-- <div class="filter-holder fixed-filter">
                            <div class="filter-button"><span>Filter</span></div>
                            <div class="gallery-filters vis-filter">
                                <a href="#" class="gallery-filter gallery-filter-active"  data-filter="*">All</a>
                                <a href="#" class="gallery-filter " data-filter=".houses">Houses</a>
                                <a href="#" class="gallery-filter" data-filter=".apartments">Apartments</a>
                                <a href="#" class="gallery-filter" data-filter=".interior">Interior</a>
                                <a href="#" class="gallery-filter" data-filter=".design">Design</a>
                            </div>
                            <div class="count-folio">
                                <div class="num-album"></div>
                                <div class="all-album"></div>
                            </div>
                        </div> -->
                        <!-- filter end-->
                        <!-- gallery-items   -->
                        <div class="gallery-items grid-no-pad hid-por-info popup-gallery">
                            <?php
                               if (isset($galleries) and $galleries != false) {
                                    foreach ($galleries as $gallery) {
                            ?>
                            <!-- 1 -->
                            <div class="gallery-item houses apartments">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="<?php echo $gallery->file->url . $gallery->file->file_name;?>"   alt="Firenze">
                                        <div class="overlay"></div>
                                        <a href="<?php echo $gallery->file->url . $gallery->file->file_name;?>" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <?php
                                }   }
                            ?>
                            <!-- 1 end -->
                            <!-- 2 -->
                            <!-- <div class="gallery-item houses interior">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/2.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/2.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 2 end -->
                            <!-- 3 -->
                            <!-- <div class="gallery-item  apartments interior">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/22.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/22.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 3 end -->
                            <!-- 4 -->
                            <!-- <div class="gallery-item  interior design">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/3.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/3.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                        <div class="overlay"></div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 4 end -->
                            <!-- 5 -->
                            <!-- <div class="gallery-item houses design">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/5.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/5.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                        <div class="overlay"></div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 5 end -->
                            <!-- 6 -->
                            <!-- <div class="gallery-item houses">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/6.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/6.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 6 end -->
                            <!-- 7 -->
                            <!-- <div class="gallery-item interior design">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/7.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/7.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 7 end -->
                            <!-- 8 -->
                            <!-- <div class="gallery-item apartments interior">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/8.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/8.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 8 end -->
                            <!-- 9 -->
                            <!-- <div class="gallery-item houses design">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/9.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/9.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 9 end -->
                            <!-- 10 -->
                            <!-- <div class="gallery-item design">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/10.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/10.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 10 end -->
                            <!-- 11 -->
                            <!-- <div class="gallery-item apartments interior">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/11.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/11.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 11 end -->
                            <!-- 12 -->
                            <!-- <div class="gallery-item houses">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/12.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/12.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 12 end -->
                            <!-- 13 -->
                            <!-- <div class="gallery-item design interior">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/13.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/13.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 13 end -->
                            <!-- 14 -->
                            <!-- <div class="gallery-item apartments">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/21.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/21.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                            </div> -->
                            <!-- 14 end -->
                            <!-- 15 -->
                            <!-- <div class="gallery-item design">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/4.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/4.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 15 end -->
                            <!-- 16 -->
                            <!-- <div class="gallery-item houses">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/16.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/16.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 16 end -->
                            <!-- 17 -->
                            <!-- <div class="gallery-item houses">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/17.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/17.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 17 end -->
                            <!-- 17 -->
                            <!-- <div class="gallery-item apartments">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/20.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/20.jpg" class="popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 17 end -->
                        </div>
                        <!-- end gallery items -->
                    </div>
                    <!-- content end-->
                </div>
                <!-- content-holder end -->
            </div>
            <!-- wrapper end -->
            <!--to top    -->
            <div class="to-top">
                <i class="fa fa-long-arrow-up"></i>
            </div>
            <!-- to top  end -->
        </div>
        <!-- Main end -->
        <!--  loader end -->
        <div class="loader">
            <div class="speeding-wheel"></div>
        </div>
        <!-- loader end -->
        <!--=============== google map ===============-->
        <script type="text/javascript"  src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <!--=============== scripts  ===============-->
        <script type="text/javascript"  src="js/jquery.min.js"></script>
        <script type="text/javascript"  src="js/plugins.js"></script>
        <script type="text/javascript"  src="js/core.js"></script>
        <script type="text/javascript"  src="js/scripts.js"></script>
    </body>

</html>