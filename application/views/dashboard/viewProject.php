<div class="modal-header">
    <h3 class="modal-title text-center" id="modal-title">{{items.name}}</h3>
</div>
<div class="modal-body" id="modal-body">
    <div class="lightBoxGallery">
        <a class="example-image-link" href="{{file.url + file.file_name}}" data-lightbox="{{items.name}}" data-title=""  ng-repeat="file in items.files">
            <img src="{{file.url + file.file_name}}" width="100px">
        </a>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
    <button class="btn btn-danger" type="button" ng-click="cancel()">Cancel</button>
</div>