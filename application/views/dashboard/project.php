<div class="wrapper wrapper-content animated fadeInRight"  >
    <div class="row" ng-show="showform">
        <div class="" ng-class="{'col-lg-12' : !files, 'col-lg-9' : files.length > 0}">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add project</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" ng-submit="addProject()" >
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Title</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.title}">
                                <input type="text" placeholder="Project title" class="form-control" ng-model="newproject.title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Category</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.category_id}">
                            <select name="category" ng-model=newproject.category_id class="form-control">
                                    <option value="" selected>Select category</option>
                                    <option  ng-repeat="category in categories" value="{{category.id}}">
                                    {{category.category}}
                                    </option>
                                </select>
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="col-lg-1 control-label">Brief Description</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.brief_description}">
                                <textarea name="brief_description" class="form-control" placeholder="Type here" ng-model="newproject.brief_description"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Description</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.description}">
                                <textarea name="description" class="form-control" placeholder="Type here" ng-model="newproject.description"></textarea>
                            </div>
                        </div>
                      
                        <div class="form-group">
                            <label for="" class="control-label col-lg-1">Date</label>
                            <div class="col-lg-5">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="open2()"><i class="glyphicon glyphicon-calendar"></i></button>
                                    </span>
                                    <input type="text" class="form-control" name="date" uib-datepicker-popup= "dd-MM-yyyy" ng-model= "newproject.date" is-open="popup2.opened" datepicker-options= "dateOptions"  ng-required="true" close-text="Close" readonly show-button-bar= "false"/>
                                </div>
                            </div>
                        </div>

                        <!--  <div class="form-group">
                            <label for="" class="control-label col-lg-1">Date</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.date}">
                                <input name="date" class="form-control" type="date" placeholder="date" ng-model="newproject.date">
                            </div>
                            
                        </div> -->

                       <div class="form-group">
                            <label class="col-lg-1 control-label">Client</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.client}">
                                <input name="client" class="form-control" placeholder="client name" ng-model="newproject.client">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Location</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.location}">
                                <input name="client" class="form-control" placeholder="Location" ng-model="newproject.location">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-1 control-label">Contact No.</label>
                            <div class="col-lg-11"  ng-class="{'has-error' : validationError.phone}">
                                <input name="phone" class="form-control" placeholder="Phone Number" ng-model="newproject.phone">
                            </div>
                        </div>
                        <div class="form-group" ng-class="{'has-error' : validationError.file}">
                            <label for="" class="control-label col-lg-1">Photo</label>
                            <div class="col-md-11">
                                <button ngf-select="uploadFiles($files, $invalidFiles)"
                                        accept="image/*"
                                        ngf-max-height="5000"
                                        ngf-max-size="5MB"
                                        ngf-multiple="true" type="button"
                                        class="upload-drop-zone btn-default"
                                        ngf-drop="uploadFiles($files)"
                                        ngf-drag-over-class="'drop'" ngf-multiple="false"
                                        ngf-pattern="'image/*'"
                                        ng-class="{'upload-drop-zone-error' : validationError.file}">
                                    <div class="dz-default dz-message">
                                        <span><strong>Drop files here or click to upload. </strong></span>
                                    </div>
                                </button>
                                <div class="help-block" ng-show="validationError.file">Please select image!</div>
                                <span class="alert alert-danger" ng-show="fileValidation.status == true">{{fileValidation.msg}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-primary" type="submit" ng-bind="(curproject == false ? 'Add' : 'Update')">Add</button>
                                <button class="btn btn-danger" type="button" ng-click="hideForm()">Cancel</button>
                            </div>
                        </div>
                    </form>
                    <div class="lightBoxProject" ng-show="curproject">
                        <div ng-repeat="file in curproject.files" class="col-md-2">
                            <a class="example-image-link" href="{{file.url + file.file_name}}" data-lightbox="item-list-{{curproject.name}}" data-title="" >
                                <img src="{{file.url + file.file_name}}" width="100px">
                            </a>
                            <a ng-click="deleteImage(file)">delete</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3" ng-if="files.length > 0">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Upload status</h5>
                </div>
                <div class="ibox-content">
                    <div ng-repeat="file in files">
                        <h5>{{file.name}}</h5>
                        <div class="lightBoxProject">
                            <a class="example-image-link" href="{{file.$ngfBlobUrl}}" data-lightbox="example-1" data-title="">
                                <img ngf-src="file.$ngfBlobUrl" alt=""  style="width: 25px; max-height: 25px" id="myImg"/>
                            </a>
                        </div>
                        <div class="progress">
                            <div aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar"
                                 class="progress-bar progress-bar-success"
                                 style="width:{{file.progress}}%" ng-show="uploadstatus != 1">
                                <span>{{file.progress}}% Complete</span>
                            </div>
                        </div>
                    </div>
                    <p class="text-danger" ng-repeat="f in errFiles">{{file.name}} {{file.$error}} {{file.$errorParam}}.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Show All projects</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-times"></i>
                        </a>
                        
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="">
                        <button type="button" class="btn btn-primary" ng-click="newProject()">
                            Add a new project
                        </button>
                    </div>
                    <div class="table-responsive">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="dataTables_length" id="DataTables_Table_0_length">
                                <label>
                                    <select  aria-controls="DataTables_Table_0" class="form-control input-sm" ng-model="numPerPage"
                                             ng-options="num for num in paginations">{{num}}
                                    </select>
                                    entries
                                </label>
                            </div>
                            <div id="DataTables_Table_0_filter" class="dataTables_filter">
                                <label>Search:<input class="form-control input-sm" placeholder="" ng-model="search" aria-controls="DataTables_Table_0" type="search"></label>
                            </div>
                            <table class="table table-striped table-bordered table-hover dataTable" id="DataTables_Table_0" aria-describedby="DataTables_Table_0_info" role="grid">
                                <thead>
                                <tr role="row">
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 151.217px;" aria-sort="ascending" >Sl No</th>
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 219.583px;">Title</th>
                                   <!--  <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 219.583px;">Category</th> -->
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 189.75px;">Brief Description</th>
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 219.583px;">Description</th>
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 151.383px;">Date</th>
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 189.75px;">Client</th>
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 219.583px;">Location</th>
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 189.75px;">Contact No.</th>
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 189.75px;">Image</th>
                                    <th tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" style="width: 151.383px;">Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr class="gradeA odd" role="row" dir-paginate="project in projects | filter:search | limitTo:pageSize | itemsPerPage:numPerPage"  current-page="currentPage">

                                    <td class="sorting_1">{{$index+1}}</td>
                                    <td>{{project.title}}</td>
                                    <!-- <td>{{project.category.category}}</td> -->
                                    <!-- <td>{{project.category_id}} -->
                                    <td>{{project.brief_description}}</td>
                                    <!-- <td ><p style="overflow: auto;width: 200px;height: 80px; border: 1px black;">{{project.description}}</p></td> -->
                                    <td><p uib-popover="{{project.description}}" style="cursor:pointer;">{{project.description | limitTo: 70 }}</p></td>
                                    <td >{{project.date | date:'dd-MM-yyyy'}}</td>
                                    <td >{{project.client}}</td>
                                    <td >{{project.location}}</td>
                                    <td >{{project.phone}}</td>
                                    <td class="center">
                                        <a class="example-image-link" href="{{project.files[0].url + project.files[0].file_name}}" data-lightbox="example-1" data-title="">
                                            <img src="{{project.files[0].url + project.files[0].file_name}}" alt=""  style="width: 25px; max-height: 25px" id="myImg"/>
                                        </a>
                                        <a href="" class="btn btn-xs btn-bitbucket" ng-click="showProject(project)" ng-if="project.files.length > 0">Show all</a>
                                    </td>
                                    <td class="center">
                                        <div  class="btn-group btn-group-xs" role="group">
                                            <button type="button" class="btn btn-info" ng-click="editProject(project)">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button  type="button" class="btn btn-danger" ng-click="deleteProject(project)">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="col-md-4">

                                <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite" ng-if="currentPage == 1">
                                    Showing {{currentPage}} to {{(numPerPage < projects.length  ? currentPage*numPerPage :projects.length)}} of {{projects.length}} entries
                                </div>
                                <div class="dataTables_info" id="datatable-buttons_info" role="status" aria-live="polite" ng-if="currentPage != 1">
                                    Showing {{(currentPage-1)*numPerPage+1}} to {{(currentPage*numPerPage)}} of {{projects.length}} entries
                                </div>
                            </div>



                            <div class="col-md-8 pull-right">
                                <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                    <dir-pagination-controls
                                        max-size="5"
                                        direction-links="true"
                                        boundary-links="true">
                                    </dir-pagination-controls>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
