<!DOCTYPE HTML>
<html lang="en">
<head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>About Firenze - Responsive  Architecture Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/yourstyle.css">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body>
        <!--================= main start ================-->
        <div id="main">
            <!--=============== header ===============-->
            <header class="main-header">
                <div class="scroll-holder">
                    <!-- logo-->
                    <div class="logo-holder">
                        <a href="index" class="ajax"><img src="images/logo.png" alt=""></a>
                        <p>Creative Theme for Architects</p>
                    </div>
                    <!-- logo end -->
                    <!-- navigation-->
                    <div class="nav-holder">
                        <nav>
                            <ul>
                                <li>
                                    <a href="index" class="ajax act-link">Home</a>
                                </li>
                                <li>
                                    <a href="about" class="ajax">About us </a>
                                </li>
                                <li>
                                    <a href="services" class="ajax">Services</a>
                                </li>
                                <li>
                                    <a href="projects" class="ajax">Projects</a>
                                </li>
                                <li>
                                    <a href="gallery" class="ajax">Gallery</a>
                                </li>
                                <li>
                                    <a href="team" class="ajax">Team</a>
                                </li>
                                <li>
                                    <a href="contact" class="ajax">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- navigation  end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Contact info</h3>
                        <ul class="header-contacts">
                            <li><span>Adress</span><a href="#">27th Brooklyn New York, NY 10065</a></li>
                            <li><span>Call</span><a href="#">+3 (123) 8976541</a></li>
                            <li><span>Write</span><a href="#">yourmail@domain.com</a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Find us</h3>
                        <ul class="header-social">
                            <li><a href="#" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i></a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <span class="close-menu"><i class="fa fa-times" aria-hidden="true"></i></span>
                </div>
                <!--footer-->
                <div class="header-footer">
                    &#169; 2018 / Designed by <a target="_blank" href="http://www.cloudbery.com/"><img src="images/cloudbery.png"></a><br>
                </div>
                <!-- footer end -->
            </header>
            <!-- header end -->
            <!-- nav-button-holder end -->
            <div class="nav-button-holder">
                <div class="nav-button">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!-- mobile logo-->
                <a class="mob-logo ajax" href="index"><img src="images/small-logo.png" alt=""></a>
            </div>
            <!-- nav-button-holder end -->
            <!--=============== wrapper ===============-->
            <div id="wrapper">
                <!-- content-holder  -->
                <div class="content-holder scale-bg2">
                    <!-- top-bar-holder  -->
                    <div class="top-bar-holder">
                        <div class="container">
                            <div class="top-bar">
                                <div class="top-bar-title">
                                    <h2><span>Page</span> : <a href="about" class="ajax">About us</a></h2>
                                </div>
                                <div class="show-share">
                                    <span>Share</span>
                                    <i class="fa fa-chain-broken"></i>
                                </div>
                                <div class="share-container"  data-share="['facebook','pinterest','googleplus','twitter','linkedin']"><a class="closeshare"><i class="fa fa-times"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <!-- top-bar-holder end  -->
                    <!-- wrapper-inner  -->
                    <div class="wrapper-inner">
                        <!-- content  -->
                        <div class="content">
                            <!-- page-title  -->
                            <div class="page-title">
                                <div class="overlay"></div>
                                <div class="slider-mask"></div>
                                <div class="bg"  style="background-image:url(images/bg/3.jpg)"></div>
                                <h2 data-top-bottom="transform: translateY(-150px);" data-bottom-top="transform: translateY(150px);">About us</h2>
                                <div class="page-title-inner">
                                    <div class="container">
                                        <div class="page-title-decor">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- page-title end -->
                            <!-- container -->
                            <div class="container mr-bottom">
                                <!-- section  -->
                                <section class="md-padding"  id="sec1">
                                    <div class="section-title">
                                        <div class="container small-container">
                                            <h3>Who we are</h3>
                                            <h4>Morbi mattis ex non urna condimentum, eget eleifend diam molestie. Curabitur lorem enim, maximus non nulla sed . </h4>
                                        </div>
                                    </div><!-- 
                                    <div class="custom-slider-holder">
                                        <div class="custom-slider">
                                            <div class="item"> -->
                                                <!-- <img src="images/folio/slider/1.jpg" class="img-responsive" alt="" class="respimg"> -->
                                            <!-- </div>
                                            <div class="item">
                                                <img src="images/folio/slider/2.jpg" alt="" class="respimg">
                                            </div>
                                            <div class="item">
                                                <img src="images/folio/slider/3.jpg" alt="" class="respimg">
                                            </div>
                                        </div>
										<div class="panel-dots-holder"><div class="panel-dots-inner"><div class="panel-dots"></div></div></div>
                                        <div class="customNavigation">
                                            <a class="prev-slide transition"><i class="fa fa-angle-left"></i></a>
                                            <a class="next-slide transition"><i class="fa fa-angle-right"></i></a>
                                        </div>
                                    </div> -->
                                    <div class="clearfix"></div>
                                    <div class="container small-container">
                                        <img src="images/folio/slider/1.jpg" class="img-responsive" alt="" class="respimg">
                                        <div class="inner-wrap">
                                            <h3>We are creative architecture Studio </h3>
                                            <p class="big-parag">Morbi mattis ex non urna condimentum, eget eleifend diam molestie. Curabitur lorem enim, maximus non nulla sed, egestas venenatis felis. </p>
                                            <p>Curabitur lorem enim, maximus non nulla sed, egestas venenatis felis. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                                            <p>
											 <!-- facts -->
                                            <div class="inline-facts-holder ">
                                                <!-- 1 -->
                                                <div class="inline-facts ">
                                                    <div class="milestone-counter">
                                                        <div class="stats animaper">
                                                            <div class="num" data-content="461" data-num="461">0</div>
                                                        </div>
                                                    </div>
                                                    <span class="count-dec">1 .</span>
                                                    <h6>Finished projects</h6>
                                                </div>
                                                <!-- 1 end  -->
                                                <!-- 2 -->
                                                <div class="inline-facts ">
                                                    <div class="milestone-counter">
                                                        <div class="stats animaper">
                                                            <div class="num" data-content="168" data-num="168">0</div>
                                                        </div>
                                                    </div>
                                                    <span class="count-dec">2 .</span>
                                                    <h6>Happy customers</h6>
                                                </div>
                                                <!-- 2 end  -->
                                                <!-- 3 -->
                                                <div class="inline-facts ">
                                                    <div class="milestone-counter">
                                                        <div class="stats animaper">
                                                            <div class="num" data-content="222" data-num="222">0</div>
                                                        </div>
                                                    </div>
                                                    <span class="count-dec">3 .</span>
                                                    <h6>Working hours</h6>
                                                </div>
                                                <!-- 3 end  -->
                                                <!-- 4 -->
                                                <div class="inline-facts">
                                                    <div class="milestone-counter">
                                                        <div class="stats animaper">
                                                            <div class="num" data-content="143" data-num="143">0</div>
                                                        </div>
                                                    </div>
                                                    <span class="count-dec">4 .</span>
                                                    <h6>Happy Clients</h6>
                                                </div>
                                                <!-- 4 end  -->
                                            </div>
											<!-- facts end-->
                                        </div>
                                    </div>
                                </section>
                                <!-- section  end-->

                                <!-- section  resume / story-->
                                <section class="md-padding" id="sec3">
                                    <div class="container small-container">
                                        <div class="section-title">
                                            <h3>Our story</h3>
                                        </div>
                                        <div class="resume-holder">
                                            <?php
                                                if (isset($testimonials) and $testimonials != false) {
                                                    foreach($testimonials as $testimonial) {
                                            ?>
                                            <!-- TESTIMONIAL ITEM -->                                            
                                            <div class="resume-item">
                                                <div class="resume-head">
                                                    <h3>2012</h3>
                                                </div>
                                                <div class="resume-box">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <img src="<?php echo $testimonial->file->url . $testimonial->file->file_name;?>" alt="" class="respimg">
                                                        </div>
                                                        <div class="col-md-9">
                                                            <h5><?php echo $testimonial->name;?></h5>
                                                            <p><?php echo $testimonial->description;?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- TESTIMONIAL ITEM END -->
                                            <?php
                                                }   }
                                            ?>    
                                        </div>
                                    </div>
                                </section>
                                <!-- section end -->

                            </div>
                            <!-- container end -->
                        </div>
                        <!-- content end -->
                    </div>
                    <!-- wrapper-inner  end -->

                </div>
                <!-- content-holder end -->
            </div>
            <!-- wrapper end -->
            <!--to top    -->
            <div class="to-top">
                <i class="fa fa-long-arrow-up"></i>
            </div>
            <!-- to top  end -->
        </div>
        <!-- Main end -->
        <!--  loader end -->
        <div class="loader">
            <div class="speeding-wheel"></div>
        </div>
        <!-- loader end -->
        <!--=============== google map ===============-->
        <script type="text/javascript"  src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <!--=============== scripts  ===============-->
        <script type="text/javascript"  src="js/jquery.min.js"></script>
        <script type="text/javascript"  src="js/plugins.js"></script>
        <script type="text/javascript"  src="js/core.js"></script>
        <script type="text/javascript"  src="js/scripts.js"></script>
    </body>
</html>