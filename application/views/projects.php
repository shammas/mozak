<!DOCTYPE HTML>
<html lang="en">
    
<head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Portfolio Firenze - Responsive  Architecture Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============--> 
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/yourstyle.css">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body>
        <!--================= main start ================-->
        <div id="main">
            <!--=============== header ===============-->   
            <header class="main-header">
                <div class="scroll-holder">
                    <!-- logo-->
                    <div class="logo-holder">
                        <a href="index" class="ajax"><img src="images/logo.png" alt=""></a>
                        <p>Creative Theme for Architects</p>
                    </div>
                    <!-- logo end -->
                    <!-- navigation-->
                    <div class="nav-holder">
                        <nav>
                            <ul>
                                <li>
                                    <a href="index" class="ajax act-link">Home</a>
                                </li>
                                <li>
                                    <a href="about" class="ajax">About us </a>
                                </li>
                                <li>
                                    <a href="services" class="ajax">Services</a>
                                </li>
                                <li>
                                    <a href="projects" class="ajax">Projects</a>
                                </li>
                                <li>
                                    <a href="gallery" class="ajax">Gallery</a>
                                </li>
                                <li>
                                    <a href="team" class="ajax">Team</a>
                                </li>
                                <li>
                                    <a href="contact" class="ajax">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- navigation  end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Contact info</h3>
                        <ul class="header-contacts">
                            <li><span>Adress</span><a href="#">27th Brooklyn New York, NY 10065</a></li>
                            <li><span>Call</span><a href="#">+3 (123) 8976541</a></li>
                            <li><span>Write</span><a href="#">yourmail@domain.com</a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Find us</h3>
                        <ul class="header-social">
                            <li><a href="#" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i></a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <span class="close-menu"><i class="fa fa-times" aria-hidden="true"></i></span>
                </div>
                <!--footer-->
                <div class="header-footer">
                    &#169; 2018 / Designed by <a target="_blank" href="http://www.cloudbery.com/"><img src="images/cloudbery.png"></a><br>
                </div>
                <!-- footer end -->
            </header>
            <!-- header end -->
            <!-- nav-button-holder end -->
            <div class="nav-button-holder">
                <div class="nav-button">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!-- mobile logo-->
                <a class="mob-logo ajax" href="index"><img src="images/small-logo.png" alt=""></a> 
            </div>
            <!-- nav-button-holder end -->
            <!--=============== wrapper ===============-->
            <div id="wrapper">
                <!-- content-holder  -->
                <div class="content-holder scale-bg2">
                    <!-- top-bar-holder  -->
                    <div class="top-bar-holder pdtb">
                        <div class="top-bar">
                            <div class="top-bar-title">
                                <h2><span>Page</span> : <a href="portfolio" class="ajax">Our projects</a></h2>
                            </div>
                            <div class="show-share">
                                <span>Share</span>
                                <i class="fa fa-chain-broken"></i>            
                            </div>
                            <div class="share-container"  data-share="['facebook','pinterest','googleplus','twitter','linkedin']"><a class="closeshare"><i class="fa fa-times"></i></a></div>
                        </div>
                    </div>
                    <!-- top-bar-holder  end-->
                    <!-- content -->
                    <div class="content pad-con">
                        <!-- filter -->
                        <div class="filter-holder inline-filter">
                            <div class="filter-button"><span>Filter</span></div>
                            <div class="gallery-filters vis-filter">
                                <a href="#" class="gallery-filter gallery-filter-active"  data-filter="*">All</a>
                                <?php
                                    if (isset($categories) and $categories != false) {
                                        foreach($categories as $category) {
                                ?>
                                <a href="#" class="gallery-filter " data-filter=".<?php echo $category->id;?>"><?php echo $category->category;?></a>
                               <!--  <a href="#" class="gallery-filter" data-filter=".apartments">Apartments</a>
                                <a href="#" class="gallery-filter" data-filter=".interior">Interior</a>
                                <a href="#" class="gallery-filter" data-filter=".design">Design</a> -->
                                <?php  
                                    }      
                                        }   
                                 ?>
                            </div>
                            <div class="count-folio">
                                <div class="num-album"></div>
                                <div class="all-album"></div>
                            </div>
                        </div>
                        <!-- filter -->
                        <!-- gallery-items   -->
                        <div class="gallery-items grid-small-pad det-vis">
                        <?php
                            if (isset($projects) and $projects != false) {
                                foreach ($projects as $project){
                                    if (isset($project->files) and $project->files != false) {
                                        $i = 0;
                                        foreach ($project->files as $key => $file) {
                                          if ($i++ < 1) {   
                        ?>
                            <!-- 1 -->

                             <div class="gallery-item <?php echo str_replace(' ', '-',  $project->category_id);?>">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="<?php echo $file->url . $file->file_name;?>"   alt="Firenze">
                                        <div class="overlay"></div>
                                        <a href="<?php echo $file->url . $file->file_name;?>" class="image-popup popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="grid-item">
                                        <h3><a href="<?php echo base_url('portfolio-single/' . $project->id. '/') . rtrim(str_replace(['.', ',', ' ', ';', '--'], '-', $project->title), '-'); ?>" class="ajax portfolio-link"><?php echo $project->title;?></a></h3>
                                        <span><?php echo $project->category->category; ?></span> 
                                    </div>
                                </div>
                            </div>
                            <!-- 1 end -->
                            <?php
                                }   }   }
                                        }   }
                            ?>
                            <!-- 2 -->
                            <!-- <div class="gallery-item houses interior">
                                <div class="grid-item-holder">
                                    <div class="box-item">
                                        <img  src="images/folio/thumbs/2.jpg"   alt="">
                                        <div class="overlay"></div>
                                        <a href="images/folio/thumbs/2.jpg" class="image-popup popup-image"><i class="fa fa-arrows-alt" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="grid-item">
                                        <h3><a href="portfolio-single" class="ajax portfolio-link">Theatre de Stoep</a></h3>
                                        <span>Hoses  / Interior</span>
                                    </div>
                                </div>
                            </div> -->
                            <!-- 2 end -->
                            
                        </div>
                        <!-- end gallery items -->                     
                            <section class="no-padding mr-bottom no-bg  pr-not">
                                                    <!-- container -->  
                        <div class="container">
                                <div class="notifer">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3>Ready to order your project ? </h3>
                                        </div>
                                        <div class="col-md-4"><a href="contact" class="ajax btn flat-btn">Get in Touch</a></div>
                                    </div>
                                </div>
                        </div>
                        <!-- container end-->   
                            </section>
    
                    </div>
                    <!-- content end-->
                </div>
                <!-- content-holder end -->
            </div>
            <!-- wrapper end -->
            <!--to top    -->
            <div class="to-top">
                <i class="fa fa-long-arrow-up"></i>
            </div>
            <!-- to top  end -->
        </div>
        <!-- Main end -->
        <!--  loader end -->
        <div class="loader">
            <div class="speeding-wheel"></div>
        </div>
        <!-- loader end -->
        <!--=============== google map ===============-->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  
        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/plugins.js"></script>
        <script type="text/javascript" src="js/core.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
    </body>

</html>