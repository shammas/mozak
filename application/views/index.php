<!DOCTYPE html>
<html lang="en-US">
    
<head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>Firenze - Responsive  Architecture Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/yourstyle.css">
        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="images/favicon.ico">
    </head>
    <body>
        <!--================= main start ================-->
        <div id="main">
            <!--=============== header ===============-->
            <header class="main-header">
                <div class="scroll-holder">
                    <!-- logo-->
                    <div class="logo-holder">
                        <a href="index" class="ajax"><img src="images/logo.png" alt=""></a>
                        <p>Creative Theme for Architects</p>
                    </div>
                    <!-- logo end -->
                    <!-- navigation-->
                    <div class="nav-holder">
                        <nav>
                            <ul>
                                <li>
                                    <a href="index" class="ajax act-link">Home</a>
                                </li>
                                <li>
                                    <a href="about" class="ajax">About us </a>
                                </li>
                                <li>
                                    <a href="services" class="ajax">Services</a>
                                </li>
                                <li>
                                    <a href="projects" class="ajax">Projects</a>
                                </li>
                                <li>
                                    <a href="gallery" class="ajax">Gallery</a>
                                </li>
                                <li>
                                    <a href="team" class="ajax">Team</a>
                                </li>
                                <li>
                                    <a href="contact" class="ajax">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- navigation  end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Contact info</h3>
                        <ul class="header-contacts">
                            <li><span>Adress</span><a href="#">27th Brooklyn New York, NY 10065</a></li>
                            <li><span>Call</span><a href="#">+3 (123) 8976541</a></li>
                            <li><span>Write</span><a href="#">yourmail@domain.com</a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <!-- header-widget-->
                    <div class="header-widget">
                        <h3>Find us</h3>
                        <ul class="header-social">
                            <li><a href="#" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#" target="_blank" ><i class="fa fa-tumblr"></i></a></li>
                        </ul>
                    </div>
                    <!-- header-widget end -->
                    <span class="close-menu"><i class="fa fa-times" aria-hidden="true"></i></span>
                </div>
                <!--footer-->
                <div class="header-footer">
                    &#169; 2018 / Designed by <a target="_blank" href="http://www.cloudbery.com/"><img src="images/cloudbery.png"></a><br>
                </div>
                <!-- footer end -->
            </header>
            <!-- header end -->
            <!-- nav-button-holder end -->
            <div class="nav-button-holder">
                <div class="nav-button">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <!-- mobile logo-->
                <a class="mob-logo ajax" href="index"><img src="images/small-logo.png" alt=""></a>
            </div>
            <!-- nav-button-holder end -->
            <!--=============== wrapper ===============-->
            <div id="wrapper">
                <!-- content-holder  -->
                <div class="content-holder scale-bg2">
                    <!-- full-height-wrap -->
                    <div class="full-height-wrap">
                        <!-- hero-wrap  -->
                        <div class="hero-wrap">
                            <!-- full-screen-slider-holder  -->
                            <div class="full-screen-slider-holder no-margin blur-slider">
                                <!-- full-screen-slider -->
                                <div class="full-screen-slider">
                                    <!-- 1 -->
                                    <div class="full-screen-item" >
                                        <div class="bg"  data-bg="images/bg/2.jpg"></div>
                                        <div class="slider-mask"></div>
                                        <div class="overlay"></div>
                                        <div class="hero-title almt">
                                            <h3>Who we are</h3>
                                            <div class="clearfix"></div>
                                            <div class="separator trsp-separator"></div>
                                            <h2>Creative Architecture  Agensy</h2>
                                            <a href="about" class="ajax btn trsp-btn">Discover our studio</a>
                                        </div>
                                    </div>
                                    <!-- 1 end-->
                                    <!-- 2 -->
                                    <div class="full-screen-item" >
                                        <div class="bg"  data-bg="images/bg/33.jpg"></div>
                                        <div class="slider-mask"></div>
                                        <div class="overlay"></div>
                                        <div class="hero-title almt">
                                            <h3>Category Hoses</h3>
                                            <div class="clearfix"></div>
                                            <div class="separator trsp-separator"></div>
                                            <h2>Project Architecture title</h2>
                                            <a href="portfolio-single" class="ajax btn trsp-btn">View Project</a>
                                        </div>
                                    </div>
                                    <!-- 2 end-->
                                    <!-- 3 -->
                                    <div class="full-screen-item" >
                                        <div class="bg"  data-bg="images/bg/1.jpg"></div>
                                        <div class="slider-mask"></div>
                                        <div class="overlay"></div>
                                        <div class="hero-title almt">
                                            <h3>What we do</h3>
                                            <div class="clearfix"></div>
                                            <div class="separator trsp-separator"></div>
                                            <h2>We Create Architecture  projects</h2>
                                            <a href="portfolio" class="ajax btn trsp-btn">Our portfolio</a>
                                        </div>
                                    </div>
                                    <!-- 3 end-->
                                    <!-- 4 -->
                                    <div class="full-screen-item" >
                                        <div class="bg"  data-bg="images/bg/3.jpg"></div>
                                        <div class="slider-mask"></div>
                                        <div class="overlay"></div>
                                        <div class="hero-title almt">
                                            <h3>Who we are</h3>
                                            <div class="clearfix"></div>
                                            <div class="separator trsp-separator"></div>
                                            <h2>Creative Architecture  Agensy</h2>
                                            <a href="about" class="ajax btn trsp-btn">Discover our studio</a>
                                        </div>
                                    </div>
                                    <!-- 4 end-->
                                </div>
                                <!-- full-screen-slider end-->
                                <div class="num-holder"></div>
                                <div class="panel-dots-holder">
                                    <div class="panel-dots-inner">
                                        <div class="panel-dots"></div>
                                    </div>
                                </div>
                                <div class="customNavigation">
                                    <a class="prev-slide transition"><i class="fa fa-angle-left"></i></a>
                                    <a class="next-slide transition"><i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                            <!-- full-screen-slider-holder end-->
                        </div>
                        <!-- hero-wrap  end-->
                    </div>
                    <!-- full-height-wrap end -->
                </div>
                <!-- content-holder end -->
            </div>
            <!-- wrapper end -->
            <!--to top    -->
            <div class="to-top">
                <i class="fa fa-long-arrow-up"></i>
            </div>
            <!-- to top  end -->
        </div>
        <!-- Main end -->
        <!--  loader end -->
        <div class="loader">
            <div class="speeding-wheel"></div>
        </div>
        <!-- loader end -->
        <!--=============== google map ===============-->
        <script type="text/javascript"  src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <!--=============== scripts  ===============-->
        <script type="text/javascript"  src="js/jquery.min.js"></script>
        <script type="text/javascript"  src="js/plugins.js"></script>
        <script type="text/javascript"  src="js/core.js"></script>
        <script type="text/javascript"  src="js/scripts.js"></script>
    </body>

</html>